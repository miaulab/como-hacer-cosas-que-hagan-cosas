/*
 *  Actividad - HACER PARPADEAR UN LED
 *
 *  Encender y apagar un LED a intervalos de 1 segundo.
 */

void setup()
{
  pinMode(13, OUTPUT);    //Inicializar el pin como salida
}

void loop()
{
  digitalWrite(13, HIGH); // Encender el LED
  delay(1000);            // Esperar un segundo
  digitalWrite(13, LOW);  // Apagar el LED
  delay(1000);            // Esperar un segundo
}
