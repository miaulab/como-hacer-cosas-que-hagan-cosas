/*
 * Actividad - PIEZOELÉCTRICO (THEREMIN)
 *
 * Genera un tono a través del altavoz piezoeléctrico mientras el botón
 * esté pulsado. La altura del tono depende del fotoresistor: a más luz,
 * más grave es el tono; a menos luz recibe, más agudo.
 */

// Constantes para los PINes donde vamos a conectar cada elemento.
const int PIN_PIEZO = 13;
const int PIN_BOTON = 7;
const int PIN_FOTO  = 5;

// Variables auxiliares
int valorFoto;
int tonoNuevo;


void setup() {
  // Inicializamos los PINes que van al polo positivo del altavoz
  // piezoeléctrico y al botón pulsador.
  pinMode(PIN_PIEZO, OUTPUT);
  pinMode(PIN_BOTON, INPUT);
}


void loop() {

  // Si el botón no está pulsado, silenciamos el tono.
  if( digitalRead( PIN_BOTON ) == LOW ){
    noTone( PIN_PIEZO );
  }
  else{
    // Si no, hacemos corresponder el tono directamente con el voltaje leído.
    // Lee voltaje entre 0V y 5V; Devuelve número entre 0 y 1023.
    valorFoto = analogRead( PIN_FOTO );
    // Trasladamos el valor al rango [440,1760]: [A4,A6]
    tonoNuevo = map( valorFoto, 0, 1023, 440, 1760);
    tone( PIN_PIEZO, tonoNuevo);
  }

}
