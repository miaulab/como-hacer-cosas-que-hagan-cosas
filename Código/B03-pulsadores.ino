/*
 *  Actividad - BOTÓN PULSADOR
 *
 *  Usar dos botones para apagar y encender un LED.
 */

// Primero definimos unas constantes para cada PIN.
// De esta manera será más fácil entender el código
const int PIN_BOTON_1 = 2;  // PIN Pulsador 1
const int PIN_BOTON_2 = 3;  // PIN Pulsador 2
const int PIN_LED =  13;    // PIN del LED


void setup() {
  // Establecer los PINs de los botones como entrada
  pinMode(PIN_BOTON_1, INPUT);
  pinMode(PIN_BOTON_2, INPUT);

  // Establecer el PIN del LED como salida
  pinMode(PIN_LED, OUTPUT);
}


void loop() {
  // Variables que almacenan el estado de cada botón
  bool estadoB1, estadoB2;

  // Leemos el estado de los botones.
  // Están pulsados si el valor leído es LOW.
  estadoB1 = digitalRead(PIN_BOTON_1) == LOW;
  estadoB2 = digitalRead(PIN_BOTON_2) == LOW;

  // Si pulsamos el botón 1 ó 2, pero no los dos a la vez, entonces...
  if ( xor(estadoB1, estadoB2) ){
    // El botón 1 enciende el LED y el 2 lo apaga
    if( estadoB1 ){
      digitalWrite(PIN_LED, HIGH);
    }
    else{
      digitalWrite(PIN_LED, LOW);
    }
  }
}

bool xor(bool a, bool b){
  // XOR viene de eXclusive OR u "O exclusivo".
  // Se refiere a la operación lógica:
  // "el uno o el otro, pero no los dos a la vez"
  return ( a || b ) && !( a && b );
}
