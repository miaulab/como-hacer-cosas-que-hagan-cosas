/*
 *  Actividad - PIEZOELÉCTRICO (SIMPLE)
 *
 *  Hacer sonar un altavoz piezoeléctrico a una frecuencia concreta.
 */

void setup() {
  // Conectamos el polo positivo del piezoeléctrico al PIN 9
  pinMode(9, OUTPUT);
  // Generamos un tono a 440 Hz en el PIN 9
  tone(9, 440);
}


void loop() {
  // En este programa todo se hace una vez, al comienzo,
  // y por eso este bucle está vacío.
}
