/*
 *  Actividad - POTENCIÓMETRO
 *
 *  Medir la posición de un potenciómetro y usarlo para
 *  controlar la velocidad de parpadeo de un LED.
 */

// El Potenciómetro está conectado al pin analógico 0
const int PIN_SENSOR = 0;

// El LED está conectado al pin digital 13
const int PIN_LED = 13;


void setup()
{
  // Configuramos PIN_LED como pin de salida.
  pinMode(PIN_LED, OUTPUT);
  // El pin analógico no se configura. Siempre es de entrada.
}


void loop()
{
  int sensorValue;

  // Leemos un valor entre 0 y 1023 (de 0 a 5 V)
  sensorValue = 1 + analogRead(PIN_SENSOR);

  // Encendemos el LED
  digitalWrite(PIN_LED, HIGH);

  // Esperamos según el valor
  // del potenciómetro (en milisegundos)
  delay(sensorValue);

  // Apagamos el LED
  digitalWrite(PIN_LED, LOW);

  // Esperamos de nuevo
  delay(sensorValue);
}
