/*
 *  Actividad - FOTORRESISTOR
 *
 *  Controlar la intensidad de un LED con un fotorresistor
 */

// Empezaremos definiendo los pines donde conectaremos
// el fotorresistor y el diodo LED
const int PIN_SENSOR = 0;
const int PIN_LED = 9;

// Tambien definiremos los umbrales de apagado y encendido
int lightLevel, high = 250, low = 700;


void setup() {
  // Definimos el PIN del LED como salida
  pinMode(PIN_LED, OUTPUT);
}


void loop() {
  // Leemos el PIN del fotorresistor
  lightLevel = analogRead(PIN_SENSOR);
  lightLevel = map(lightLevel, low, high, 255, 0);
  lightLevel = constrain(lightLevel, 0, 255);

  // Ajustamos luminosidad del diodo en función de los valores obtenidos
  analogWrite(ledPin, lightLevel);
}
