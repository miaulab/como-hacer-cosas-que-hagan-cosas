/*
 *  Actividad - PIEZOELÉCTRICO (BOTONES)
 *
 *  Usar varios botones pulsadores y un piezoeléctrico a modo de pianillo.
 */

const int PIN_PIEZO = 7;

// Variables para recordar el tono seleccionado
int tonoActual = 0;
int tonoNuevo  = 0;

// Variables para almacenar el estado de los botones (pulsado/no-pulsado)
bool b8  = false;
bool b9  = false;
bool b10 = false;
bool b11 = false;
bool b12 = false;

// Contaremos el número de botones que se pulsan simultáneamente
int contador = 0;

void setup() {

  // Inicializamos el PIN del piezoeléctrico como salida
  pinMode(PIN_PIEZO, OUTPUT);

  // Usaremos los PINes del 8 al 12 para conectar los pulsadores,
  // así que los configuramos como entradas.
  pinMode(8,INPUT);
  pinMode(9,INPUT);
  pinMode(10,INPUT);
  pinMode(11,INPUT);
  pinMode(12,INPUT);
}


// Esta función se ejecuta una y otra vez, después de la inicialización.
void loop() {

  // Damos a las variables un valor por defecto: ningún botón pulsado
  contador = 0;
  b8 = b9 = b10 = b11 = b12 = false;

  // Comprobamos qué botones están pulsados.
  // Como ves, usamos resistencias pull-up.
  if( digitalRead(8) == LOW ){
    b8 = true;
    contador += 1;
  }
  if( digitalRead(9) == LOW ){
    b9 = true;
    contador += 1;
  }
  if( digitalRead(10) == LOW ){
    b10 = true;
    contador += 1;
  }
  if( digitalRead(11) == LOW ){
    b11 = true;
    contador += 1;
  }
  if( digitalRead(12) == LOW ){
    b12 = true;
    contador += 1;
  }

  // Si no se pulsa ningún botón, paramos el tono.
  if( contador == 0 ){
    noTone(PIN_PIEZO);
    tonoActual = 0;
  }

  // ¿Para qué será esto...? =)
  if( contador > 3 ){
    oniudra();
    return;
  }

  // Guardamos el nuevo tono en la variable.
  // Hemos asignado a cada botón una nota musical distinta.
  if( b8 ){
    tonoNuevo = 262; // C4
  }
  else if( b9 ){
    tonoNuevo = 294; // D4
  }
  else if( b10 ){
    tonoNuevo = 330; // E4
  }
  else if( b11 ){
    tonoNuevo = 349; // F4
  }
  else if( b12 ){
    tonoNuevo = 392; // G4
  }

  // Solo lo cambiamos si es distinto al que había antes.
  if( tonoNuevo != tonoActual ){
    tonoActual = tonoNuevo;
    tone(PIN_PIEZO,tonoNuevo);
  }
}



void nota( unsigned int frecuencia, unsigned int milisegundos ){
  tone(PIN_PIEZO, frecuencia, milisegundos);
  delay(milisegundos);
}

void silencio( unsigned int milisegundos ){
  noTone(PIN_PIEZO);
  delay(milisegundos);
}

void oniudra(){
  nota(294,113); silencio(20);
  nota(523,113); silencio(20);
  nota(523,113); silencio(20);
  nota(523,113); silencio(20);
  nota(494,113); silencio(20);
  nota(440,113); silencio(20);
  nota(392,180); silencio(20);
  nota(294,180); silencio(20);
  nota(98,400);
  silencio(2000);
  return;
}
