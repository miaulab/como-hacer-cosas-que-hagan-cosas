/*
 *  Actividad - MOTOR CORRENTE CONTÍNUA
 *
 */

// Definimos el PIN al que conectamos el motor
const int motorPin = 9;

void setup() {
	// Definimos el PIN del motor como salida
	pinMode(motorPin, OUTPUT);
	Serial.begin(9600);
}

void loop() {
	motorAcceleration();
}

void motorAcceleration() {
	int speed;
	int delayTime = 20;

	// Acelerar el motor
	for(speed = 0; speed <= 255; speed++) {
		analogWrite(motorPin,speed);
		delay(delayTime);
	}

	// Decelerar el motor
	for(speed = 255; speed >= 0; speed--) {
		analogWrite(motorPin,speed);
		delay(delayTime);
	}
}